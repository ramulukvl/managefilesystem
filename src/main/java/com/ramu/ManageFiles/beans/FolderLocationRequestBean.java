package com.ramu.ManageFiles.beans;

public class FolderLocationRequestBean {
	
	private String folderPath;

	public String getFolderPath() {
		return folderPath;
	}

	public void setFolderPath(String folderPath) {
		this.folderPath = folderPath;
	}

}
