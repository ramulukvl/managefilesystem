package com.ramu.ManageFiles.beans;

import java.util.List;

public class FileListResponse {
	
	private List<String> folderDataList;
	
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<String> getFolderDataList() {
		return folderDataList;
	}

	public void setFolderDataList(List<String> folderDataList) {
		this.folderDataList = folderDataList;
	}

}
