package com.ramu.ManageFiles.controller.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ramu.ManageFiles.beans.FileListResponse;
import com.ramu.ManageFiles.beans.FolderLocationRequestBean;
import com.ramu.ManageFiles.controller.FileSystemController;
import com.ramu.ManageFiles.service.FileSystemService;

@Controller
@RequestMapping("/filesystem")
public class FileSystemControllerImpl implements FileSystemController {

	@Autowired
	private FileSystemService fileSystemServiceImpl;
	
	@Override
	@RequestMapping(value = "list", method = RequestMethod.POST)
	public @ResponseBody FileListResponse getFolderContentList(@RequestBody FolderLocationRequestBean folder ) {

		return fileSystemServiceImpl.getFileList(folder.getFolderPath());

	}

	public FileSystemService getService() {
		return fileSystemServiceImpl;
	}

	public void setService(FileSystemService service) {
		this.fileSystemServiceImpl = service;
	}

}