package com.ramu.ManageFiles.controller;

import com.ramu.ManageFiles.beans.FileListResponse;
import com.ramu.ManageFiles.beans.FolderLocationRequestBean;

public interface FileSystemController {

	public FileListResponse getFolderContentList(FolderLocationRequestBean folder);

}