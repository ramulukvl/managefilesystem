package com.ramu.ManageFiles.service;

import com.ramu.ManageFiles.beans.FileListResponse;

public interface FileSystemService {

	public FileListResponse getFileList(String location);

}