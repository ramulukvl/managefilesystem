package com.ramu.ManageFiles.service.impl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.ramu.ManageFiles.beans.FileListResponse;
import com.ramu.ManageFiles.service.FileSystemService;

@Service
public class FileSystemServiceImpl implements FileSystemService {

	/* (non-Javadoc)
	 * @see com.ramu.ManageFiles.service.impl.FileSystemService#getFileList(java.lang.String)
	 */
	@Override
	@SuppressWarnings("unused")
	public FileListResponse getFileList(String location) {
		FileListResponse response = new FileListResponse();
		response.setFolderDataList(new ArrayList<String>());
		File currentDir = new File(location);
		if (currentDir == null || !currentDir.exists()) {
			response.setMessage("Given Path does not exist");
		} else {
			this.displayDirectoryContents(currentDir, response);
		}
		return response;
	}

	public void displayDirectoryContents(File dir, FileListResponse response) {
		try {
			File[] files = dir.listFiles();
			for (File file : files) {
				if (file.isDirectory()) {
					response.getFolderDataList().add(file.getCanonicalPath());
					// displayDirectoryContents(file, response);
				} else {
					response.getFolderDataList().add(file.getCanonicalPath());
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
